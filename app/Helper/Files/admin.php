<?php   


if (!function_exists('adminAuth')) 
{
    function adminAuth()
    {
        return auth()->guard('admin')->user();
    }
}




// define link for assets of admin ( styles and scripts )
if (!function_exists('aurl')) 
{
    function aurl()
    {
         return url('admin');
    }
}