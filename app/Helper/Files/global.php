<?php


// display image from uploads folder
if (!function_exists('getImage')) 
{
    function getImage($path, $name) 
    {
        return url('uploads/'. $path. '/'. $name);
    }
}

if (!function_exists('getSmallImage')) 
{
    function getSmallImage($path, $name) 
    {
        return url('uploads/'. $path. 'small/'. $name);
    }
}

if (!function_exists('getMediumImage')) 
{
    function getMediumImage($path, $name) 
    {
        return url('uploads/'. $path. 'medium/'. $name);
    }
}



// pathes    

define("UPLOADS_PATH", 'uploads/');
define("SLIDER_PATH", 'slider/');
define("PLACES_PATH", 'places/');
define("AGENTS_PATH", 'agents/');
define("PROPS_PATH", 'properties/');
define("ABOUT_PATH", 'about/');
define("LANG_PATH", 'lang/');
define("SETTINGS_PATH", 'settings/');
define("OFFERS_PATH", 'offers/');
