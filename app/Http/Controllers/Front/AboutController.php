<?php

namespace App\Http\Controllers\Front;

use App\Models\About;
use App\Models\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        $data['about'] = About::first();
        $data['agents'] = Agent::select('name', 'slug', 'img', 'email', 'phone')
        ->where('show_in_homepage', 'yes')
        ->get();        
        
        return view('front.about.index')->with($data);
    }
}
