<?php

namespace App\Http\Controllers\Front;

use App\Models\Agent;
use App\Models\AgentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentRequestRequest;

class AgentController extends Controller
{
    public function index()
    {
        $data['agents'] = Agent::select('name', 'slug', 'pos', 'small_desc', 'img', 'email', 'phone', 'facebook', 'linkedin')
        ->where('show', 'yes')
        ->get();
        
        return view('front.agents.index')->with($data);
    }

    public function profile($slug)
    {
        $data['agent'] = Agent::where('slug', $slug)->first();

        return view('front.agents.profile')->with($data);
    }

    public function sendMessage(AgentRequestRequest $request)
    {
        if($request->ajax())
        {
            $data = $request->validated();
            AgentRequest::create($data);
            $message['success'] = trans('Message sent successfully');
            return response()->json($message);
        }
    }
}
