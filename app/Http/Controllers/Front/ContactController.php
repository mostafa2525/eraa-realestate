<?php

namespace App\Http\Controllers\Front;

use App\Models\Message;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;

class ContactController extends Controller
{
    public function index()
    {
        $data['contacts'] = Setting::select('email', 'mobile', 'linkedin', 'facebook', 'business_hours')
        ->first();
        return view('front.contact.index')->with($data);
    }

    public function sendMessage(MessageRequest $request)
    {
        if($request->ajax())
        {
            $data = $request->validated();
            Message::create($data);
            $message['success'] = trans('Message sent successfully');
            return response()->json($message);
        }
    }
}
