<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests\SubscribeRequest;
use App\Http\Requests\LoadMoreRequest;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Place;
use App\Models\Agent;
use App\Models\Prop;
use App\Models\Setting;
use App\Models\Subscribe;
use App\Models\Offer;
use View;

class HomeController extends Controller
{
    public function index()
    {
        $data['sliders'] = Slider::select('name', 'img', 'price', 'link')
        ->where('show', 'yes')
        ->get();

        $data['offers'] = Offer::select('name', 'sub_title', 'img', 'link')
        ->where('show_in_homepage', 'yes')
        ->get();

        $data['agents'] = Agent::select('name', 'slug', 'img', 'email', 'phone')
        ->where('show_in_homepage', 'yes')
        ->get();

        $data['places'] = Place::select('id', 'name', 'img', 'slug')
        ->where('show_in_homepage', 'yes')
        ->limit(6)
        ->get();

        $data['props'] = Prop::select('type', 'price', 'place_id', 'img', 'slug', 'beds', 'baths', 'lot_size')
        ->where('show_in_homepage', 'yes')
        ->limit(9)
        ->get();

        return view('front.index')->with($data);
    }

    public function loadMore(LoadMoreRequest $request)
    {
        if($request->ajax())
        {
            $data = $request->validated();

            if($data['num'] == 3) 
            {
                $data['props_two'] = Prop::select('type', 'price', 'place_id', 'img', 'slug', 'beds', 'baths', 'lot_size')
                ->where('show_in_homepage', 'yes')->skip(3)->take(3)->get();
                
                $view = View::make('front.render.propTwo', $data);
            }
            else if($data['num'] == 6) 
            {
                $data['props_three'] = Prop::select('type', 'price', 'place_id', 'img', 'slug', 'beds', 'baths', 'lot_size')
                ->where('show_in_homepage', 'yes')->skip(6)->take(3)->get();
                
                $view = View::make('front.render.propThree', $data);
            }
            

            $html = $view->render();
            
            return response()->json($html);
        }
    }

    public function subscribe(SubscribeRequest $request)
    {
        if($request->ajax())
        {
            $data = $request->validated();
            Subscribe::create($data);
            $message['success'] = trans('Subscribed successfully');
            return response()->json($message);
        }
    }
}
