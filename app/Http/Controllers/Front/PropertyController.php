<?php

namespace App\Http\Controllers\Front;

use App\Models\Prop;
use App\Models\Agent;
use App\Models\Place;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;

class PropertyController extends Controller
{
    public function index(SearchRequest $request)
    {
        $params = $request->validated();

        if($params !== [])
        {
            $data['status'] = 'search';
            $data['search'] = true;
            $data['params'] = $params;

            $props = Prop::when($params['cat'] !== null, function ($props) {
                    return $props->where('category_id', request('cat'));
                })
                ->when($params['place'] !== null, function ($props) {
                    return $props->where('place_id', request('place'));
                })
                ->when($params['type'] !== null, function ($props) {
                    return $props->where('type', request('type'));
                })
                ->when($params['min_price'] !== null, function ($props) {
                    return $props->where('price', '>=', request('min_price'));
                })
                ->when($params['max_price'] !== null, function ($props) {
                    return $props->where('price', '<=', request('max_price'));
                });

            $data['props'] = $props->paginate(6);
        } else {
            $data['status'] = 'all';
            $data['props'] = Prop::paginate(6);
        }

        
        return view('front.properties.index')->with($data);
    }

    public function sell()
    {
        $data['props'] = Prop::where('type', 'sell')->paginate(6);
        $data['count'] = count(Prop::where('type', 'sell')->get());
        $data['status'] = 'sell';

        return view('front.properties.index')->with($data);
    }

    public function rent()
    {
        $data['props'] = Prop::where('type', 'rent')->paginate(6);
        $data['count'] = count(Prop::where('type', 'rent')->get());
        $data['status'] = 'rent';

        return view('front.properties.index')->with($data);
    }

    public function place($slug)
    {
        $place = Place::where('slug', $slug)->first();
        $data['place'] = $place;
        $data['props'] = Prop::where('place_id', $place->id)->paginate(6);
        $data['count'] = count(Prop::where('type', 'rent')->get());
        $data['status'] = 'place';

        return view('front.properties.index')->with($data);
    }


    public function show($slug)
    {
        $data['prop'] = Prop::where('slug', $slug)->first();
        return view('front.properties.show')->with($data);
    }
}
