<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cat' => 'nullable|exists:categories,id',
            'place' => 'nullable|exists:places,id',
            'type' => 'nullable|string|in:sell,rent',
            'min_price' => 'nullable|integer',
            'max_price' => 'nullable|integer',
        ];
    }
}
