<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = [
        'lang_id', 'name', 'slug', 'img', 'email', 'phone', 'pos', 'small_desc', 'desc',
        'facebook', 'linked_in', 'twitter', 'instagram', 'youtube', 'show', 'show_in_homepage'
    ];

    public function props()
    {
        return $this->belongsToMany('App\Models\Prop');
    }

    public function requests()
    {
        return $this->hasMany('App\Models\AgentRequest', 'agent_id', 'id');
    }

}
