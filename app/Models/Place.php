<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = [
        'lang_id', 'name', 'img', 'slug', 'desc', 'seo', 'show_in_homepage', 'show'
    ];

    public function props()
    {
        return $this->hasMany('App\Models\Prop', 'place_id', 'id');
    }

}
