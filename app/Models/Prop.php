<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prop extends Model
{
    // protected $fillable = [
    //     'lang_id', 'place_id', 'name', 'img', 'desc', 'price', 'code', 'address', 'neighborhode',
    //     'beds', 'baths', 'garages', 'living_area', 'lot_size', 'year_built', 'type', 'show',
    //     'show_in_homePage', 'seo', 'slug'
    // ];

    protected $guarded = ['id'];

    public function place()
    {
        return $this->hasOne('App\Models\Place', 'id', 'place_id');
    }

    public function agents()
    {
        return $this->belongsToMany('App\Models\Agent');
    }
    
    public function images()
    {
        return $this->hasMany('App\Models\PropImage', 'prop_id', 'id');
    }

    public function features()
    {
        return $this->hasMany('App\Models\PropFeature', 'prop_id', 'id');
    }

    public function requests()
    {
        return $this->hasMany('App\Models\PropRequest', 'prop_id', 'id');
    }

}
