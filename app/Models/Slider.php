<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'lang_id', 'name', 'price', 'img', 'link', 'show'
    ];
}
