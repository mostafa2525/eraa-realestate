<?php

namespace App\Providers;

use App\Models\Lang;
use App\Models\Place;
use App\Models\Setting;
use App\Models\SearchOption;
use Illuminate\Support\ServiceProvider;
use App\Models\Category;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('front.inc.header', function($view){
            $view->with('settings', Setting::first());
            $view->with('langs', Lang::all());
            $view->with('options', SearchOption::first());
            $view->with('places', Place::all());
            $view->with('cats', Category::all());
        });

        view()->composer('front.properties.index', function($view){
            $view->with('options', SearchOption::first());
            $view->with('places', Place::all());
            $view->with('cats', Category::all());
        });

        // view()->composer('front.properties.search', function($view){
        //     $view->with('options', SearchOption::first());
        //     $view->with('places', Place::all());
        //     $view->with('cats', Category::all());
        // });

        view()->composer('front.inc.footer', function($view){
            $view->with('settings', Setting::first());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
