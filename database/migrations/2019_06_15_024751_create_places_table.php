<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lang_id')->nullable();
            $table->string('name');
            $table->string('img');
            $table->string('slug')->unique();
            $table->text('desc')->nullable();
            $table->text('seo')->nullable();
            $table->enum('show_in_homePage',['yes','no'])->default('yes');
            $table->enum('show',['yes','no'])->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
