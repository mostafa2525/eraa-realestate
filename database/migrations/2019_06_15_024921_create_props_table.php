<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('props', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lang_id')->nullable();
            $table->bigInteger('place_id')->unsigned();
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
            $table->string('name');
            $table->bigInteger('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->string('img');
            $table->text('desc')->nullable();
            $table->integer('price');
            $table->string('code')->nullable();
            $table->string('address')->nullable();
            $table->string('neighborhood')->nullable();
            $table->tinyInteger('beds')->nullable();
            $table->tinyInteger('baths')->nullable();
            $table->tinyInteger('garages')->nullable();
            $table->integer('living_area')->nullable();
            $table->integer('lot_size')->nullable();
            $table->string('year_built')->nullable();
            $table->enum('type',['sell','rent'])->default('sell');
            $table->enum('show',['yes','no'])->default('yes');
            $table->enum('show_in_homePage',['yes','no'])->default('yes');
            $table->text('seo')->nullable();
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('props');
    }
}
