<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prop_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prop_id')->unsigned();
            $table->foreign('prop_id')->references('id')->on('props')->onDelete('cascade');
            $table->string('name');
            $table->string('email');
            $table->string('subject')->nullable();
            $table->string('message');
            $table->string('mobile')->nullable();
            $table->text('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prop_requests');
    }
}
