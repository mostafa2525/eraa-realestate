<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lang_id')->nullable();
            $table->bigInteger('cat_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('img');
            $table->text('small_desc')->nullable();
            $table->text('desc')->nullable();
            $table->text('seo')->nullable();
            $table->string('slug')->nullable();
            //  view or not ( home page  or site )
            $table->enum('show_in_homePage',['yes','no'])->default('yes');
            $table->enum('show',['yes','no'])->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
