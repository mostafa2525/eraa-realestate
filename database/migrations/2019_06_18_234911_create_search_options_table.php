<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('min_beds_start');
            $table->integer('min_beds_stop');
            $table->bigInteger('min_price_start');
            $table->bigInteger('min_price_step');
            $table->bigInteger('min_price_stop');
            $table->bigInteger('max_price_start');
            $table->bigInteger('max_price_step');
            $table->bigInteger('max_price_stop');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_options');
    }
}
