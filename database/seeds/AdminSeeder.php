<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Admin();
        $data->name = "admin";
        $data->email = "admin@en.com";
        $data->password = bcrypt('123456');
        $data->save();
    }
}
