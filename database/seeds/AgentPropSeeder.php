<?php

use Illuminate\Database\Seeder;
use App\Models\Agent;
use App\Models\Prop;
use Faker\Factory as Faker;

class AgentPropSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $agentNum = count(Agent::select('id')->get());
        $propNum = count(Prop::select('id')->get());

        foreach (range(1, $agentNum) as $i) {
            DB::table('agent_prop')->insert([
                ['agent_id' => $i, 'prop_id' => $faker->numberBetween(1, $propNum/3)],
                ['agent_id' => $i, 'prop_id' => $faker->numberBetween($propNum/3 + 1, $propNum * (2/3))],
                ['agent_id' => $i, 'prop_id' => $faker->numberBetween($propNum * (2/3) + 1, $propNum)],
            ]);
        }
    }
}
