<?php

use Illuminate\Database\Seeder;
use App\Models\Agent;
use Faker\Factory as Faker;

class AgentSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $i) {

            $name = $faker->name('male');
            $show_in_homepage = $faker->boolean(60)? 'yes' : 'no';

            Agent::create([
                'name' => $name,
                'slug' => Str::slug($name),
                'img' => $faker->numberBetween(1,3). '.jpg',
                'email' => $faker->email,
                'phone' => $faker->tollFreePhoneNumber,
                'pos' => 'Real Estate Broker',
                'small_desc' => $faker->text(30),
                'desc' => $faker->text(150),
                'linkedin' => 'https://linkedin.com',
                'facebook' => 'https://facebook.com',
                'show' => 'yes',
                'show_in_homepage' => $show_in_homepage
            ]);
        }
    }
}
