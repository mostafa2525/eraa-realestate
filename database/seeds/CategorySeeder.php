<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use Faker\Factory as Faker;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        Category::create([
            'name' => 'apartment',
            'desc' => $faker->text(100)
        ]);

        Category::create([
            'name' => 'house',
            'desc' => $faker->text(100)
        ]);

        Category::create([
            'name' => 'villa',
            'desc' => $faker->text(100)
        ]);

        Category::create([
            'name' => 'chalet',
            'desc' => $faker->text(100)
        ]);
    }
}
