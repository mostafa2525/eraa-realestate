<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LangSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(SearchOptionSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(AgentSeeder::class);
        $this->call(PlaceSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(PropSeeder::class);
        $this->call(AgentPropSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(OfferSeeder::class);
    }
}
