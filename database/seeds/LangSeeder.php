<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class LangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lang::create([
            'name' => 'English',
            'symbole' => 'en',
            'icon' => 'en.png'
        ]);

        Lang::create([
            'name' => 'Arabic',
            'symbole' => 'ar',
            'icon' => 'ar.png'
        ]);
    }
}
