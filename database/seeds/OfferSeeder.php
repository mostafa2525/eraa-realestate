<?php

use Illuminate\Database\Seeder;
use App\Models\Offer;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Offer::create([
            'name'  => 'Amazing Collections',
            'sub_title' => 'From: $100,000',
            'img' => '1.jpg',
            'link' => '#',
            'show' => 'yes',
            'show_in_homepage' => 'yes' 
        ]);

        Offer::create([
            'name'  => 'Exclusive Comdominium',
            'sub_title' => 'Miami Beach',
            'img' => '2.jpg',
            'link' => '#',
            'show' => 'yes',
            'show_in_homepage' => 'yes' 
        ]);
    }
}
