<?php

use Illuminate\Database\Seeder;
use App\Models\Place;
use Faker\Factory as Faker;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,10) as $i) {

            $name = $faker->city;
            $show_in_homepage = $faker->boolean(30)? 'yes' : 'no';

            Place::create([
                'name' => $name,
                'img' => $faker->numberBetween(1,3). '.jpg',
                'slug' => Str::slug($name),
                'desc' => $faker->text(100),
                'show_in_homepage' => $show_in_homepage,
                'show' => 'yes'
            ]);
        }
    }
}
