<?php

use Illuminate\Database\Seeder;
use App\Models\Prop;
use App\Models\PropImage;
use App\Models\PropFeature;
use Faker\Factory as Faker;

class PropSeeder extends Seeder
{
    public function run()
    {

        $faker = Faker::create();
        $props = ['Air conditioning', 'Home Theater', 'Central Heating', 'Laundry', 'Balcony'];

        foreach (range(1,30) as $i) {

            $name = $faker->secondaryAddress. ' '. $faker->state;

            if($faker->boolean(50) == true)
            {
                $type = 'rent';
                $mul = 100;
            } else {
                $type = 'sell';
                $mul = 100000;
            }

            $show_in_homepage = $faker->boolean(30)? 'yes' : 'no';


            Prop::create([
                'place_id' => $faker->numberBetween(1, 10),
                'name' => $name,
                'category_id' => $faker->numberBetween(1, 4),
                'img' => $faker->numberBetween(1, 3). '.jpg',
                'desc' => $faker->text(100),
                'price' => $faker->numberBetween(10,50) * $mul,
                'code' => '#'. $faker->numerify('######'),
                'address' => $faker->streetAddress,
                'neighborhood' => $faker->city,
                'beds' => $faker->numberBetween(1,10),
                'baths' => $faker->numberBetween(1,10),
                'garages' => $faker->numberBetween(1,5),
                'living_area' => $faker->numberBetween(100,1000),
                'lot_size' => $faker->numberBetween(1000,10000),
                'year_built' => $faker->year,
                'type' => $type,
                'show' => 'yes',
                'show_in_homepage' => $show_in_homepage,
                'slug' => Str::slug($name),
            ]);
    
            foreach (range(4, 6) as $n) {
                PropImage::create([
                    'prop_id' => $i,
                    'image' => $n. '.jpg',
                ]);
            }    
    
            foreach (range(0, sizeof($props)-1) as $n) {
                PropFeature::create([
                    'prop_id' => $i,
                    'name' => $props[$n],
                ]);
            }
    
        }
        
                
    }
}
