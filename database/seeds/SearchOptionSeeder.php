<?php

use Illuminate\Database\Seeder;
use App\Models\SearchOption;

class SearchOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SearchOption::create([
            'min_beds_start' => 1,
            'min_beds_stop' => 10,
            'min_price_start' => 1000000,
            'min_price_step' => 500000,
            'min_price_stop' => 5000000,
            'max_price_start' => 1000000,
            'max_price_step' => 500000,
            'max_price_stop' => 5000000,
        ]);
    }
}
