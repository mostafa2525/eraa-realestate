<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Setting();
        $data->site_name = "eraa real-estate";
        $data->email = "info@real-estate.com";
        $data->facebook = "https://www.facebook.com/";
        $data->twitter = "https://twitter.com/";
        $data->instagram = "https://www.instagram.com/";
        $data->youtube = "https://www.youtube.com/";
        $data->linkedin = "https://www.linkedin.com/";
        $data->fax = "123456";
        $data->mobile = "(800) 123-4567";
        $data->phone = "(800) 123-4567";
        $data->address = "1234 Street Name, City Name";
        $data->logo = "logo.png";
        $data->whatsapp = "(800) 123-4567";
        $data->business_hours = 'Monday - Friday - 9am to 5pm';
        $data->save();
    }
}
