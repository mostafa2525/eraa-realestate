<?php

use Illuminate\Database\Seeder;
use App\Models\Slider;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::create([
            'name' => 'PORTO DRIVE',
            'price' => '$975,000',
            'img' => '1.jpg',
            'link' => '#',
            'show' => 'yes'
        ]);

        Slider::create([
            'name' => 'SOUTH MIAMI',
            'price' => '$790,000',
            'img' => '2.jpg',
            'link' => '#',
            'show' => 'yes'
        ]);

        Slider::create([
            'name' => 'MIAMI AVE',
            'price' => '$625,000',
            'img' => '3.jpg',
            'link' => '#',
            'show' => 'yes'
        ]);
    }
}
