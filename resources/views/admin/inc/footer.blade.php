
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        2018 - 2019 &copy; Greeva theme by <a href="#">EraaSoft</a> 
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->



        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
        

        <!-- Vendor js -->
        <script src="{{aurl()}}/assets/js/vendor.min.js"></script>

        <!-- KNOB JS -->
        <script src="{{aurl()}}/assets/libs/jquery-knob/jquery.knob.min.js"></script>
        <!-- Chart JS -->
        <script src="{{aurl()}}/assets/libs/chart-js/Chart.bundle.min.js"></script>

        <!-- Jvector map -->
        <script src="{{aurl()}}/assets/libs/jqvmap/jquery.vmap.min.js"></script>
        <script src="{{aurl()}}/assets/libs/jqvmap/jquery.vmap.usa.js"></script>
        
        <!-- Datatable js -->
        <script src="{{aurl()}}/assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="{{aurl()}}/assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="{{aurl()}}/assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="{{aurl()}}/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        
        <!-- Dashboard Init JS -->
        <script src="{{aurl()}}/assets/js/pages/dashboard.init.js"></script>
        
        <!-- App js -->
        <script src="{{aurl()}}/assets/js/app.min.js"></script>
        
    </body>

<!-- Mirrored from coderthemes.com/greeva/layouts/horizontal/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Jun 2019 06:37:22 GMT -->
</html>