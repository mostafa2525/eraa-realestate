<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/greeva/layouts/horizontal/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Jun 2019 06:36:39 GMT -->
<head>
        <meta charset="utf-8" />
        <title>Greeva - Responsive Bootstrap 4 Admin Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{aurl()}}/assets/images/favicon.ico">

        <!-- jvectormap -->
        <link href="{{aurl()}}/assets/libs/jqvmap/jqvmap.min.css" rel="stylesheet" />

        <!-- DataTables -->
        <link href="{{aurl()}}/assets/libs/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{aurl()}}/assets/libs/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>        

        <!-- App css -->
        <link href="{{aurl()}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{aurl()}}/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{aurl()}}/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>




<!-- Navigation Bar-->
        <header id="topnav">

            @include('admin.inc.header.customNav')
            @include('admin.inc.header.menu')

        </header>
        <!-- End Navigation Bar-->


                <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">
