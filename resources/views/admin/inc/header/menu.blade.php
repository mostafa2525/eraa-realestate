<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="index.html">
                        <i class="dripicons-meter"></i>@lang('admin.dashboard')
                    </a>
                </li>

                

                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-view-list-large"></i>@lang('admin.categories') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>

        


            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->