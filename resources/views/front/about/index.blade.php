@extends('front.main')


@section('content')
 <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
    <div class="container">
      <div class="row">
        <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
          <h1 class="text-dark">{{ $about->name }}</h1>
        </div>
        <div class="col-md-4 order-1 order-md-2 align-self-center">
          <ul class="breadcrumb d-block text-md-right">
            <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
            <li><a href="#">About</a></li>
            <li class="active">{{ $about->name }}</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="container">

    <div class="row pb-5 pt-3">
      <div class="col-lg-9">
        
        <p class="lead mb-4">{{ $about->top_desc }}</p>

        <img width="400" class="img-fluid float-right ml-4 mb-4 mt-1" alt="" src="{{ getMediumImage(ABOUT_PATH, $about->img ) }}">

        <p>{{ $about->side_desc }}</p>

        <div class="row">
          <div class="col-lg-4">
            <h4 class="mt-4 mb-3">Our Mission</h4>

            <p>{{ $about->mission }}</p>
          </div>
          <div class="col-lg-4">
            <h4 class="mt-4 mb-3">Our Vision</h4>
            <p>{{ $about->vision }}</p>
          </div>
          <div class="col-lg-4 mb-4 mb-lg-0">
            <h4 class="mt-4 mb-3">Our Values</h4>

            <p>{{ $about->values }}</p>
          </div>
        </div>

      </div>
      <div class="col-lg-3">
        <aside class="sidebar">
          <div class="row">
            <div class="col-md-6 col-lg-12">	
              <div class="agents text-color-light text-center">
                <h4 class="text-light pt-4 m-0">Our Agents</h4>
                <div class="owl-carousel owl-theme nav-bottom rounded-nav pl-1 pr-1 pt-3 m-0" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': true}">
                  @foreach($agents as $agent)
                    <div class="pr-2 pl-2">
                        <a href="{{ route('front.get.agent.profile', ['slug' => $agent->slug]) }}" class="text-decoration-none">
                            <span class="agent-thumb">
                                <img class="img-fluid rounded-circle" src="{{ getSmallImage(AGENTS_PATH, $agent->img) }}" alt />
                            </span>
                            <span class="agent-infos text-light pt-3">
                                <strong class="text-uppercase font-weight-bold">{{ $agent->name }}</strong>
                                <span class="font-weight-light">{{ $agent->phone }}</span>
                                <span class="font-weight-light">{{ $agent->email }}</span>
                            </span>
                        </a>
                    </div> 
                  @endforeach     
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-12">
              <div class="newsletter box-shadow-custom p-4 text-center">
                  <i class="icon-envelope-letter icons"></i>
                  <h4 class="mt-1 mb-1">
                      Newsletter
                  </h4>
                  <p>
                      Subscribe and be the first to know about our best offers
                  </p>

                  @include('front.errors.error')

                  
                  <form method="POST" id="subscribeForm" class="text-black pt-2">
                      @csrf
                      <input class="form-control" placeholder="Your Name *" minlength="5" maxlength="30" name="name" id="newsletterName" type="text" required>
                      <input class="form-control" placeholder="Your Email Address *" minlength="5" maxlength="50" name="email" id="newsletterEmail" type="email" required>
                      <button class="btn btn-light btn-block text-uppercase bg-color-secondary mt-4 text-light custom-padding-1" type="submit">Subscribe</button>
                  </form>
              </div>
            </div>
          </div>
        </aside>
      </div>
    </div>

  </div>
@endsection

@section('scripts')
<script>
  $('#nav-about').addClass('active');

  $('#subscribeForm').submit(function(e){
    e.preventDefault();
    $('#display-errors').hide()
    $('#display-errors ul').empty()
    var formData  = new FormData(jQuery(this)[0]);
    axios.post('subscribe', {
        name: formData.get('name'),
        email: formData.get('email'),
    })
    .then(function (res) {
        toast.fire({
            type: 'success',
            title: res.data.success,
        })

        $('#subscribeForm').trigger('reset')
    })
    .catch(function (res) {
        let errors = res.response.data.errors
        $('#display-errors').show()
        for (var error in errors) {
            $('#display-errors ul').append('<li class="text-center">'+ errors[error] +'</li>')
        }
    });
  })

</script>
@endsection