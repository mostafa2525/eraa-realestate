@extends('front.main')


@section('content')

<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
    <div class="container">
      <div class="row">
        <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
          <h1 class="text-dark">Agents</h1>
        </div>
        <div class="col-md-4 order-1 order-md-2 align-self-center">
          <ul class="breadcrumb d-block text-md-right">
            <li><a href="demo-real-estate.html">Home</a></li>
            <li><a href="#">About</a></li>
            <li class="active">Agents</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="container">
    @foreach($agents as $agent)
      <div class="row agent-item">
        <div class="col-lg-2">
          <img src="{{ getSmallImage(AGENTS_PATH, $agent->img) }}" class="img-fluid" alt="">
        </div>
        <div class="col-lg-6">
          <h3 class="mt-1 mb-1">{{ $agent->name }}</h3>
          <h6 class="mb-1">{{ $agent->pos }}</h6>
          <p>
              {{ $agent->small_desc }}
          </p>
          <a class="btn btn-secondary btn-sm mt-1" href="{{ route('front.get.agent.profile', ['slug' => $agent->slug]) }}">View Profile</a>
        </div>
        <div class="col-lg-4">
          <ul class="list list-icons m-sm ml-5">
            <li>
              <a href="mailto:{{ $agent->email }}">
                <i class="icon-envelope-open icons"></i> {{ $agent->email }}
              </a>
            </li>
            <li>
              <a href="#">
                <i class="icon-call-out icons"></i> {{ $agent->phone }}
              </a>
            </li>
            <li>
              <a href="{{ $agent->linkedin }}">
                <i class="icon-social-linkedin icons"></i> Linkedin
              </a>
            </li>
            <li>
              <a href="{{ $agent->facebook }}">
                <i class="icon-social-facebook icons"></i> Facebook
              </a>
            </li>
          </ul>
        </div>
      </div>
    @endforeach
  </div>
@endsection

@section('scripts')
<script>
  $('#nav-about').addClass('active');
</script>
@endsection