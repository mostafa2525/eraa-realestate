@extends('front.main')

@section('content')
<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
  <div class="container">
    <div class="row">
      <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
        <h1 class="text-dark">{{ $agent->name }}</h1>
      </div>
      <div class="col-md-4 order-1 order-md-2 align-self-center">
        <ul class="breadcrumb d-block text-md-right">
          <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="{{ route('front.get.agent.index') }}"">Agents</a></li>
          <li class="active">{{ $agent->name }}</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<div class="container">

  <div class="row">
    <div class="col">
      <div class="agent-item agent-item-detail">
        <div class="row">
          <div class="col-lg-3">
            <img src="{{ getMediumImage(AGENTS_PATH, $agent->img) }}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-5">
            <h2 class="mt-0 mb-1 font-weight-normal text-uppercase">{{ $agent->name }}</h2>
            <h6 class="mb-1">{{ $agent->pos }}</h6>
            <div class="text-4 mt-3 mb-3">
              <p>
                {{ $agent->desc }}
              </p>
            </div>
          </div>
          <div class="col-lg-4">
            <ul class="list list-icons m-sm ml-5">
              <li>
                <a href="mailto:{{ $agent->email }}">
                  <i class="icon-envelope-open icons"></i> {{ $agent->email }}
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="icon-call-out icons"></i> {{ $agent->phone }}
                </a>
              </li>
              <li>
                <a href="{{ $agent->linkedin }}">
                  <i class="icon-social-linkedin icons"></i> Lindekin
                </a>
              </li>
              <li>
                <a href="{{ $agent->facebook }}">
                  <i class="icon-social-facebook icons"></i> Facebook
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <h4 class="mt-4">Contact Me</h4>

            @include('front.errors.error')

            <form id="contactAgentForm" class="contact-form" method="POST">
              @csrf
              <input type="hidden" name="id" value="{{ $agent->id }}">
              <div class="form-row">
                <div class="form-group col-lg-4">
                  <input type="text" placeholder="Your Name" value="" minlength="5" maxlength="30" class="form-control" name="name" id="name" required>
                </div>
                <div class="form-group col-lg-4">
                  <input type="email" placeholder="Your E-mail" value="" minlength="5" maxlength="50" class="form-control" name="email" id="email" required>
                </div>
                <div class="form-group col-lg-4">
                  <input type="text" placeholder="Subject (Optional)" value="" maxlength="100" class="form-control" name="subject" id="subject">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col">
                  <textarea maxlength="5000" placeholder="Message" rows="5" class="form-control" name="message" id="message" required></textarea>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col">
                  <input type="submit" value="Send Message" class="btn btn-secondary">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <h4 class="mt-3 mb-0">Agent Properties</h4>
      <div class="row mb-5 properties-listing sort-destination p-0">
        @foreach ($agent->props as $prop)            
          <div class="col-lg-4 p-3 isotope-item">
            <div class="listing-item">
              <a href="{{ route('front.get.property.show', ['slug' => $prop->slug]) }}" class="text-decoration-none">
                <div class="thumb-info thumb-info-lighten border">
                  <div class="thumb-info-wrapper m-0">
                    <img src="{{ getMediumImage(PROPS_PATH, $prop->img) }}" class="img-fluid" alt="">
                    <div class="thumb-info-listing-type bg-color-secondary text-uppercase text-color-light font-weight-semibold p-1 pl-3 pr-3">
                      for sale
                    </div>
                  </div>
                  <div class="thumb-info-price bg-color-primary text-color-light text-4 p-2 pl-4 pr-4">
                    $ {{ number_format($prop->price) }}
                    <i class="fas fa-caret-right text-color-secondary float-right"></i>
                  </div>
                  <div class="custom-thumb-info-title b-normal p-4">
                    <div class="thumb-info-inner text-3">{{ $prop->place->name }}</div>
                    <ul class="accommodations text-uppercase font-weight-bold p-0 mb-0 text-2">
                      <li>
                        <span class="accomodation-title">
                          Beds:
                        </span>
                        <span class="accomodation-value custom-color-1">
                            {{ $prop->beds }}
                        </span>
                      </li>
                      <li>
                        <span class="accomodation-title">
                          Baths:
                        </span>
                        <span class="accomodation-value custom-color-1">
                            {{ $prop->baths }}
                        </span>
                      </li>
                      <li>
                        <span class="accomodation-title">
                          Sq Ft:
                        </span>
                        <span class="accomodation-value custom-color-1">
                            {{ number_format($prop->lot_size) }}
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>
              </a>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>

</div>   
@endsection


@section('scripts')
<script>
  $('#nav-about').addClass('active');

  $('#contactAgentForm').submit(function(e){
        e.preventDefault();
        $('#display-errors').hide()
        $('#display-errors ul').empty()
        var formData  = new FormData(jQuery(this)[0]);
        axios.post('{{ route('front.post.agent.sendMessage') }}', {
            agent_id: formData.get('id'),
            name: formData.get('name'),
            email: formData.get('email'),
            subject: formData.get('subject'),
            message: formData.get('message') 
        })
        .then(function (res) {
            Swal.fire(
              'Success!',
              res.data.success,
              'success'
            )

            $('#contactAgentForm').trigger('reset')
        })
        .catch(function (res) {
          let errors = res.response.data.errors
            $('#display-errors').show()
            for (var error in errors) {
                $('#display-errors ul').append('<li class="text-center">'+ errors[error] +'</li>')
            }
        });

  })
</script>
@endsection