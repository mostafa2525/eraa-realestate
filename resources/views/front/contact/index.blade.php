@extends('front.main')


@section('content')
  <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
    <div class="container">
      <div class="row">
        <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
          <h1 class="text-dark">Contact</h1>
        </div>
        <div class="col-md-4 order-1 order-md-2 align-self-center">
          <ul class="breadcrumb d-block text-md-right">
            <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
            <li class="active">Contact</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="container">

    @include('_messages')

    <div class="row">
      <div class="col-lg-8">

        <h4 class="text-primary mt-4">Send a Message</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus.</p>

        @include('front.errors.error')

        <form id="contactForm" class="contact-form" method="POST">
          @csrf
          <div class="form-row">
            <div class="form-group col-lg-6">
              <label>Your name *</label>
              <input type="text" minlength="5" maxlength="30" class="form-control" name="name" id="name" required>
            </div>
            <div class="form-group col-lg-6">
              <label>Your email address *</label>
              <input type="email" minlength="5" maxlength="50" class="form-control" name="email" id="email" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col">
              <label>Message *</label>
              <textarea maxlength="5000" rows="5" class="form-control" name="message" id="message" required></textarea>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col">
              <input type="submit" value="Send Message" class="btn btn-secondary mb-4" data-loading-text="Loading...">
            </div>
          </div>
        </form>
</div>
      <div class="col-lg-4">
        <div class="row">
          <div class="col-md-6 col-lg-12 mt-lg-4 order-1">
            <h4 class="text-primary">The Office</h4>
            <ul class="list list-icons mt-3">
              <li>
                <a href="mailto:mail@domain.com">
                  <i class="icon-envelope-open icons"></i> {{ $contacts->email }}
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="icon-call-out icons"></i> {{ $contacts->mobile }}
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="icon-social-linkedin icons"></i> {{ $contacts->linkedin }}
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="icon-social-facebook icons"></i> {{ $contacts->facebook }}
                </a>
              </li>
            </ul>
          </div>
          <div class="col order-2 order-md-3 order-lg-2">
            <hr class="solid">
          </div>
          <div class="col-md-6 col-lg-12 order-3 order-md-2 order-lg-3">
            <h4 class="text-primary">Business Hours</h4>
            <ul class="list list-icons mt-3">
              <li><i class="far fa-clock"></i> {{ $contacts->business_hours }}</li>
            </ul>
          </div>
        </div>
      </div>

    </div>

  </div>

  <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
  <div id="googlemaps" class="google-map mt-5 mb-0"></div>

@endsection

@section('scripts')
<script>
  $('#nav-contact').addClass('active');

  $('#contactForm').submit(function(e){
        e.preventDefault();
        $('#display-errors').hide()
        $('#display-errors ul').empty()
        var formData  = new FormData(jQuery(this)[0]);
        axios.post('{{ route('front.post.contact.sendMessage') }}', {
            name: formData.get('name'),
            email: formData.get('email'),
            message: formData.get('message') 
        })
        .then(function (res) {
            Swal.fire(
              'Success!',
              res.data.success,
              'success'
            )

            $('#contactForm').trigger('reset')
        })
        .catch(function (res) {
          let errors = res.response.data.errors
            $('#display-errors').show()
            for (var error in errors) {
                $('#display-errors ul').append('<li class="text-center">'+ errors[error] +'</li>')
            }
        });

  })

</script>
@endsection