<footer id="footer" class="m-0 custom-bg-color-1 py-4">
    <div class="container">
        <div class="row pt-5 pb-4">
            <div class="col-md-4 col-lg-3">
                <h4 class="mb-3">Porto Real Estate</h4>
                <p class="custom-color-2 mb-0">
                    {{ $settings->address }}<br>
                    Phone : {{ $settings->phone }}<br>
                    Email : <a class="text-color-secondary" href="mailto:mail@example.com">{{ $settings->email }}</a>
                </p>
            </div>
            <div class="col-md-2">
                <h4 class="mb-3">Properties</h4>
                <nav class="nav-footer">
                    <ul class="custom-list-style-1 mb-0">
                        <li>
                            <a href="{{ route('front.get.property.sell') }}" class="custom-color-2 text-decoration-none">
                                For Sale
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('front.get.property.rent') }}" class="custom-color-2 text-decoration-none">
                                For Rent
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-2">
                <h4 class="mb-3">Links</h4>
                <nav class="nav-footer">
                    <ul class="custom-list-style-1 mb-0">
                        <li>
                            <a href="{{ route('front.get.agent.index') }}" class="custom-color-2 text-decoration-none">
                                Agents
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('front.get.about.index') }}" class="custom-color-2 text-decoration-none">
                                Who We Are
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('front.get.contact.index') }}" class="custom-color-2 text-decoration-none">
                                Contact
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-4 col-lg-5">
                <h4 class="mb-3">Latest Tweet</h4>
                <div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': 'oklerthemes', 'count': 1}">
                    <p>Please wait...</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright custom-bg-color-1 pb-0">
        <div class="container">
            <div class="row pt-3 pb-4">
                <div class="col-lg-12 left m-0 pb-3">
                    <p>© Copyright EraaSoft 2019. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</div>

<!-- Vendor -->
<script src="{{ furl() }}/assets/vendor/jquery/jquery.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="{{ furl() }}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/jquery.cookie/jquery.cookie.min.js"></script>		
<script src="{{ furl() }}/assets/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-real-estate.less"></script>		
<script src="{{ furl() }}/assets/vendor/popper/umd/popper.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/bootstrap/js/bootstrap.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/common/common.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/jquery.validation/jquery.validate.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/isotope/jquery.isotope.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/vide/jquery.vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{ furl() }}/assets/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="{{ furl() }}/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>		
<script src="{{ furl() }}/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Current Page Vendor and Views -->
<script src="{{ furl() }}/assets/js/views/view.contact.js"></script>

<!-- Demo -->
<script src="{{ furl() }}/assets/js/demos/demo-real-estate.js"></script>

<!-- Theme Custom -->
<script src="{{ furl() }}/assets/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="{{ furl() }}/assets/js/theme.init.js"></script>

<script src="{{ furl() }}/assets/js/main.js"></script>



<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-42715764-5', 'auto');
    ga('send', 'pageview');
</script>

<script src="{{ furl() }}/assets/master/analytics/analytics.js"></script>


@yield('scripts')

</body>

<!-- Mirrored from preview.oklerthemes.com/porto/7.0.0/demo-real-estate.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 15:01:39 GMT -->
</html>
