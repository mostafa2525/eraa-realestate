<!DOCTYPE html>
<html data-style-switcher-options="{'changeLogo': false, 'borderRadius': 0, 'colorPrimary': '#333b48', 'colorSecondary': '#219cd2', 'colorTertiary': '#2bca6e', 'colorQuaternary': '#FFF'}">
	
<!-- Mirrored from preview.oklerthemes.com/porto/7.0.0/demo-real-estate.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 15:00:59 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">	
        
        <meta name="csrf-token" content="{{ csrf_token() }}">


		<title>Porto - Responsive HTML5 Template 7.0.0</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/magnific-popup/magnific-popup.min.css">



		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ furl() }}/assets/css/theme.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/css/theme-elements.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/css/theme-blog.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="{{ furl() }}/assets/vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="{{ furl() }}/assets/css/demos/demo-real-estate.css">

		<!-- Skin CSS -->
        <link rel="stylesheet" href="{{ furl() }}/assets/css/skins/skin-real-estate.css">		
        <script src="{{ furl() }}/assets/master/style-switcher/style.switcher.localstorage.js"></script> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ furl() }}/assets/css/custom.css">

		<!-- Head Libs -->
		<script src="{{ furl() }}/assets/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body class="loading-overlay-showing" data-loading-overlay>
		<div class="loading-overlay">
			<div class="bounce-loader">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
        </div>
        
        <div class="body">
            @include('front.inc.header.nav')

            <div role="main" class="main">

