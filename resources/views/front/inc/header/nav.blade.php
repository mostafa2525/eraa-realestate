<header id="header" class="header-transparent-dark-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': false, 'stickyStartAt': 53, 'stickySetTop': '-53px'}">
    <div class="header-body bg-color-primary border-color-dark border-top-0">
        <div class="header-top header-top-borders header-top-light-borders">
            <div class="container h-100">
                <div class="header-row h-100">
                    <div class="header-column justify-content-start">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">
                                    <li class="nav-item nav-item-borders py-2 d-none d-sm-inline-flex">
                                        <span class="pl-0"><i class="far fa-dot-circle text-4 text-color-light" style="top: 1px;"></i> {{ $settings->address }}</span>
                                    </li>
                                    <li class="nav-item nav-item-borders py-2">
                                        <a href="tel:123-456-7890"><i class="fab fa-whatsapp text-4 text-color-light" style="top: 0;"></i> {{ $settings->whatsapp }}</a>
                                    </li>
                                    <li class="nav-item nav-item-borders py-2 d-none d-md-inline-flex">
                                        <a href="mailto:mail@domain.com"><i class="far fa-envelope text-4 text-color-light" style="top: 1px;"></i> {{ $settings->email }}</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    {{-- <div class="header-column justify-content-end">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">
                                    <li class="nav-item nav-item-borders py-2 d-none d-lg-inline-flex">
                                        <a href="#">Blog</a>
                                    </li>
                                    <li class="nav-item nav-item-borders py-2 pr-0 dropdown">
                                        <a class="nav-link" href="#" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ getImage(LANG_PATH. $langs[0]->icon) }}" class="flag flag-us" alt="English" /> {{ $langs[0]->name }}
                                            <i class="fas fa-angle-down"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                                            @foreach ($langs as $lang)                                                
                                                <a class="dropdown-item" href="#"><img src="{{ getImage(LANG_PATH. $lang->icon) }}" class="flag flag-us" alt="English" /> {{ $lang->name }}</a>
                                            @endforeach
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="header-container header-container-height-sm container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{ route('front.get.home.index') }}">
                                <img alt="Porto" width="143" height="40" src="{{ getImage(SETTINGS_PATH, $settings->logo) }}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-stripe header-nav-force-light-text header-nav-dropdowns-dark header-nav-no-space-dropdown order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="dropdown-full-color dropdown-quaternary">
                                            <a id="nav-home" class="nav-link" href="{{ route('front.get.home.index') }}">
                                                Home
                                            </a>
                                        </li>
                                        <li class="dropdown-full-color dropdown-quaternary">
                                            <a id="nav-props" class="nav-link" href="{{ route('front.get.property.index') }}">
                                                Properties
                                            </a>
                                        </li>

                                        <li class="dropdown dropdown-full-color dropdown-quaternary">
                                            <a id="nav-about" class="nav-link dropdown-toggle" href="#">
                                                About
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="{{ route('front.get.agent.index') }}">Agents</a></li>
                                                <li><a class="dropdown-item" href="{{ route('front.get.about.index') }}">Who We Are</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-full-color dropdown-quaternary">
                                            <a id="nav-contact" class="nav-link" href="{{ route('front.get.contact.index') }}">
                                                Contact
                                            </a>
                                        </li>
                                        <li class="dropdown dropdown-full-color dropdown-quaternary dropdown-mega" id="headerSearchProperties">
                                            <a class="nav-link dropdown-toggle" href="#">
                                                Search <i class="fas fa-search ml-2"></i>
                                            </a>
                                            <ul class="dropdown-menu custom-fullwidth-dropdown-menu ml-0">
                                                <li>
                                                    <div class="dropdown-mega-content mt-3 mt-lg-0">
                                                        {{-- <form id="propertiesFormNav" action="{{ route('front.post.property.search') }}" method="POST">
                                                            @csrf
                                                            <div class="container p-0">
                                                                <div class="form-row">
                                                                    <div class="form-group col-lg-2 mb-2 mb-lg-0">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase text-2" name="cat" data-msg-required="This field is required." id="propertiesPropertyType" required="">
                                                                                <option value="">Category</option>
                                                                                @foreach ($cats as $cat)                                                                                                                                                
                                                                                    <option value="{{ $cat->id }}" @isset($params) @if($params['cat'] == $cat->id) selected @endif @endisset>{{ $cat->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-lg-2 mb-2 mb-lg-0">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase text-2" name="place" data-msg-required="This field is required." id="propertiesLocation" required="">
                                                                                <option value="">Place</option>
                                                                                @foreach ($places as $place)  
                                                                                    <option value="{{ $place->id }}" @isset($params) @if($params['place'] == $place->id) selected @endif @endisset>{{ $place->name }}</option>
                                                                                @endforeach                                                                                                                                        
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-lg-2 mb-2 mb-lg-0">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase text-2" name="min_beds" data-msg-required="This field is required." id="propertiesMinBeds" required="">
                                                                                <option value="">Min Beds</option>
                                                                                @foreach (range($options->min_beds_start, $options->min_beds_stop) as $i)
                                                                                    <option value="{{ $i }}" @isset($params) @if($params['min_beds'] == $i) selected @endif @endisset>{{ $i }}</option>                                       
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-lg-2 mb-2 mb-lg-0">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase text-2" name="min_price" data-msg-required="This field is required." id="propertiesMinPrice" required="">
                                                                                <option value="">Min Price</option>
                                                                                @foreach (range($options->min_price_start, $options->min_price_stop, $options->min_price_step) as $i)
                                                                                    <option value="{{ $i }}" @isset($params) @if($params['min_price'] == $i) selected @endif @endisset>${{ number_format($i) }}</option>                                       
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-lg-2 mb-2 mb-lg-0">
                                                                        <div class="form-control-custom">
                                                                            <select class="form-control text-uppercase text-2" name="max_price" data-msg-required="This field is required." id="propertiesMaxPrice" required="">
                                                                                <option value="">Max Price</option>
                                                                                @foreach (range($options->max_price_start, $options->max_price_stop, $options->max_price_step) as $i)
                                                                                    <option value="{{ $i }}" @isset($params) @if($params['max_price'] == $i) selected @endif @endisset>${{ number_format($i) }}</option>                                       
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-lg-1 mb-2 mb-lg-0">
                                                                        <button type="submit" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
                                                                            <i class="fas fa-search"></i>
                                                                        </button>
                                                                    </div>
                                                                    <div class="col-lg-1 mb-2 mb-lg-0">
                                                                        <a type="button" href="{{ route('front.get.property.index') }}" style="color: white" class="btn btn-info btn-lg btn-block text-uppercase text-2 text-center">
                                                                            <i class="fas fa-redo"></i>
                                                                        </a> 
                                                                    </div>                                                              
                                                                </div>
                                                            </div>
                                                        </form> --}}
                                                        <form id="propertiesForm" action="{{ route('front.get.property.index') }}" method="get">
                                                            <div class="form-row">
                                                              <div class="form-group col-lg-2 mb-0">
                                                                <div class="form-control-custom mb-3">
                                                                  <select class="form-control text-uppercase text-2" name="cat" data-msg-required="This field is required."
                                                                    id="propertiesPropertyType2" >
                                                                    <option value="">Category</option>
                                                                    @foreach ($cats as $cat)  
                                                                      <option value="{{ $cat->id }}" @isset($search) @if($params['cat'] == $cat->id) selected @endif @endisset>{{ $cat->name }}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                              </div>
                                                              
                                                              <div class="form-group col-lg-2 mb-0">
                                                                <div class="form-control-custom mb-3">
                                                                  <select class="form-control text-uppercase text-2" name="place" data-msg-required="This field is required."
                                                                    id="propertiesLocation2" >
                                                                    <option value="">Place</option>
                                                                    @foreach ($places as $place)  
                                                                      <option value="{{ $place->id }}" @isset($search) @if($params['place'] == $place->id) selected @endif @endisset>{{ $place->name }}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                              </div>
                                                  
                                                              <div class="form-group col-lg-2 mb-0">
                                                                <div class="form-control-custom mb-3">
                                                                  <select class="form-control text-uppercase text-2" name="type" data-msg-required="This field is required."
                                                                    id="propertiesType2" >
                                                                    <option value="">Type</option>
                                                                    <option value="sell" @isset($search) @if($params['type'] == 'sell') selected @endif @endisset>sell</option>
                                                                    <option value="rent" @isset($search) @if($params['type'] == 'rent') selected @endif @endisset>rent</option>
                                                                  </select>
                                                                </div>
                                                              </div>
                                                  
                                                              <div class="form-group col-lg-2 mb-0">
                                                                <div class="form-control-custom mb-3">
                                                                  <input class="form-control text-uppercase text-2" name="min_price" placeholder="Min Price" @isset($search) value="{{ $params['min_price'] }}" @endisset>
                                                                </div>
                                                              </div>
                                                  
                                                              <div class="form-group col-lg-2 mb-0">
                                                                <div class="form-control-custom mb-3">
                                                                  <input class="form-control text-uppercase text-2" name="max_price" placeholder="Max Price" @isset($search) value="{{ $params['max_price'] }}" @endisset>
                                                                </div>
                                                              </div>
                                                  
                                                              <div class="form-group col-lg-2 mb-2 mb-lg-0">
                                                                <input type="submit" value="Search Now" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
                                                              </div>
                                                            </div>
                                                        </form>                                                  
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
