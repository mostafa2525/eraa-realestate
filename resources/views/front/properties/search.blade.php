@extends('front.main')


@section('content')
<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
  <div class="container">
    <div class="row">
      <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
        <h1 class="text-dark">Properties <p class="lead mb-0">
          Listings for 
          @if($status == 'all')
           Sell or Rent 
          @elseif($status == 'sell') 
            Sell 
          @elseif($status == 'rent')  
            Rent 
          @else 
            Search results
          @endif - {{ $count }} properties
        </p></h1>
      </div>
      <div class="col-md-4 order-1 order-md-2 align-self-center">
        <ul class="breadcrumb d-block text-md-right">
          <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
          <li class="active">PROPERTIES</li>
        </ul>
      </div>
    </div>
    <div class="row mt-4">
      <div class="col">
        <form id="propertiesForm" action="{{ route('front.post.property.doSearch') }}" method="POST">
          @csrf
          <div class="form-row">
            <div class="form-group col-lg-2 mb-0">
              <div class="form-control-custom mb-3">
                <select class="form-control text-uppercase text-2" name="cat" data-msg-required="This field is required."
                  id="propertiesPropertyType2" >
                  <option value="">Category</option>
                  @foreach ($cats as $cat)  
                    <option value="{{ $cat->id }}" @if($params['cat'] == $cat->id) selected @endif>{{ $cat->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            
            <div class="form-group col-lg-2 mb-0">
              <div class="form-control-custom mb-3">
                <select class="form-control text-uppercase text-2" name="place" data-msg-required="This field is required."
                  id="propertiesLocation2" >
                  <option value="">Place</option>
                  @foreach ($places as $place)  
                    <option value="{{ $place->id }}" @isset($params) @if($params['place'] == $place->id) selected @endif @endisset>{{ $place->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group col-lg-2 mb-0">
              <div class="form-control-custom mb-3">
                <select class="form-control text-uppercase text-2" name="type" data-msg-required="This field is required."
                  id="propertiesType2" >
                  <option value="">Type</option>
                  <option value="sell" @isset($params) @if($params['type'] == 'sell') selected @endif @endisset>sell</option>
                  <option value="rent" @isset($params) @if($params['type'] == 'rent') selected @endif @endisset>rent</option>
                </select>
              </div>
            </div>

            <div class="form-group col-lg-2 mb-0">
              <div class="form-control-custom mb-3">
                <input class="form-control text-uppercase text-2" name="min_price" placeholder="Min Price" @isset($params) value="{{ $params['min_price'] }}" @endisset>
              </div>
            </div>

            <div class="form-group col-lg-2 mb-0">
              <div class="form-control-custom mb-3">
                <input class="form-control text-uppercase text-2" name="max_price" placeholder="Max Price" @isset($params) value="{{ $params['max_price'] }}" @endisset>
              </div>
            </div>

            <div class="form-group col-lg-1 mb-0">
              <input type="submit" value="Search" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
            </div>
            <div class="col-lg-1 mb-0">
              <a type="button" href="{{ route('front.get.property.index') }}" style="color: white" class="btn btn-info btn-lg btn-block text-uppercase text-2">
                Refresh
              </a> 
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<div class="container">

  <div class="row mb-4 properties-listing sort-destination p-0">

    @if($count == 0)
      <div class="col-md-4 offset-md-4 mt-5 mb-5 isotope-item">
        <div class="text-center">
          <strong>No results for this options</strong>
        </div>
      </div>
    @endif

    @foreach ($props as $prop)
    <div class="col-md-6 col-lg-4 p-3 isotope-item">
        <div class="listing-item">
          <a href="{{ route('front.get.property.show', ['slug' => $prop->slug]) }}" class="text-decoration-none">
            <div class="thumb-info thumb-info-lighten border">
              <div class="thumb-info-wrapper m-0">
                <img src="{{ getImage(PROPS_PATH, $prop->img) }}" class="img-fluid" alt="">
                <div class="thumb-info-listing-type bg-color-secondary text-uppercase text-color-light font-weight-semibold p-1 pl-3 pr-3">
                  for {{ $prop->type }}
                </div>
              </div>
              <div class="thumb-info-price bg-color-primary text-color-light text-4 p-2 pl-4 pr-4">
                @if($prop->type == 'sell')
                  $ {{ number_format($prop->price) }}
                @else 
                  $ {{ number_format($prop->price) }} / month
                @endif
                <i class="fas fa-caret-right text-color-secondary float-right"></i>
              </div>
              <div class="custom-thumb-info-title b-normal p-4">
                <div class="thumb-info-inner text-3">{{ $prop->place->name }}</div>
                <ul class="accommodations text-uppercase font-weight-bold p-0 mb-0 text-2">
                  <li>
                    <span class="accomodation-title">
                      Beds:
                    </span>
                    <span class="accomodation-value custom-color-1">
                      {{ $prop->beds }}
                    </span>
                  </li>
                  <li>
                    <span class="accomodation-title">
                      Baths:
                    </span>
                    <span class="accomodation-value custom-color-1">
                        {{ $prop->baths }}                      
                    </span>
                  </li>
                  <li>
                    <span class="accomodation-title">
                      Sq Ft:
                    </span>
                    <span class="accomodation-value custom-color-1">
                        {{ $prop->lot_size }}
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </a>
        </div>
      </div>        
    @endforeach
  
  </div>

  <div class="pagination justify-content-center">
    {{ $props->render() }}
  </div>

</div>
@endsection

@section('scripts')
<script>
    $('#nav-props').addClass('active');

</script>
@endsection