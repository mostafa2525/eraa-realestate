@extends('front.main')


@section('content')
  <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
    <div class="container">
      <div class="row">
        <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
            <h1 class="text-dark">{{ $prop->name }}<p class="lead mb-0">{{ $prop->place->name }} - <a href="#map" data-hash data-hash-offset="100">(Map Location)</a></p></h1>
        </div>
        <div class="col-md-4 order-1 order-md-2 align-self-center">
          <ul class="breadcrumb d-block text-md-right">
            <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
            <li><a href="{{ route('front.get.property.index') }}">Properties</a></li>
            <li class="active">Detail</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="container">

    <div class="row pb-5 pt-3">
      <div class="col-lg-9">
            
        <div class="row">
          <div class="col-lg-7">

            <span class="thumb-info-listing-type thumb-info-listing-type-detail bg-color-secondary text-uppercase text-color-light font-weight-semibold p-2 pl-3 pr-3">
              for {{ $prop->type }}
            </span>

            <div class="thumb-gallery">
              <div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
                <div class="owl-carousel owl-theme manual thumb-gallery-detail show-nav-hover" id="thumbGalleryDetail">
                  @foreach ($prop->images as $image)                      
                    <div>
                      <a href="img/demos/real-estate/listings/listing-detail-1.jpg">
                        <span class="thumb-info thumb-info-centered-info thumb-info-no-borders text-4">
                          <span class="thumb-info-wrapper text-4">
                            <img alt="Property Detail" src="{{ getMediumImage(PROPS_PATH, $image->image) }}" class="img-fluid">
                            <span class="thumb-info-title text-4">
                              <span class="thumb-info-inner text-4"><i class="icon-magnifier icons text-4"></i></span>
                            </span>
                          </span>
                        </span>
                      </a>
                    </div>
                  @endforeach
                </div>
              </div>

              <div class="owl-carousel owl-theme manual thumb-gallery-thumbs mt" id="thumbGalleryThumbs">
                @foreach ($prop->images as $image)                                      
                  <div>
                    <img alt="Property Detail" src="{{ getSmallImage(PROPS_PATH, $image->image) }}" class="img-fluid cur-pointer">
                  </div>
                @endforeach
              </div>
            </div>

          </div>
          <div class="col-lg-5">
            
            <table class="table table-striped">
              <colgroup>
                <col width="35%">
                <col width="65%">
              </colgroup>
              <tbody>
                <tr>
                  <td class="bg-color-primary text-light align-middle">
                    Price
                  </td>
                  <td class="text-4 font-weight-bold align-middle bg-color-primary text-light">
                    @if($prop->type == 'sell')
                      $ {{ number_format($prop->price) }}
                    @else 
                      $ {{ number_format($prop->price) }} / month
                    @endif
                  </td>
                </tr>
                <tr>
                  <td>
                    Listing ID
                  </td>
                  <td>
                    {{ $prop->code }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Address
                  </td>
                  <td>
                    {{ $prop->address }}<br><a href="#map" class="text-2" data-hash data-hash-offset="100">(Map Location)</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    Neighborhood
                  </td>
                  <td>
                    {{ $prop->neighborhood }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Beds
                  </td>
                  <td>
                    {{ $prop->beds }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Baths
                  </td>
                  <td>
                    {{ $prop->baths }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Garages
                  </td>
                  <td>
                      {{ $prop->garages }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Living Area
                  </td>
                  <td>
                      {{ number_format($prop->living_area) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Lot Size
                  </td>
                  <td>
                      {{ number_format($prop->lot_size) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    Year Built
                  </td>
                  <td>
                      {{ $prop->year_built }}
                  </td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>

        <div class="row">
          <div class="col">

            <h4 class="mt-3 mb-3">Description</h4>
            <p>{!! $prop->desc !!}</p>

            <hr class="solid my-5">

            <h4 class="mt-3 mb-3">Features</h4>

            <ul class="list list-icons list-secondary row m-0">
              @foreach ($prop->features as $feature)                  
                <li class="col-sm-6 col-lg-4"><i class="fas fa-check"></i> {{ $feature->name }}</li>
              @endforeach
            </ul>

            <hr class="solid my-5">

            <h4 class="mt-3 mb-3" id="map">Map Location</h4>
            <div id="googlemaps" class="google-map mb-0"></div>

          </div>
        </div>

      </div>
      <div class="col-lg-3">
        <aside class="sidebar">
          <div class="agents text-color-light text-center pb-3">
            <h4 class="text-light pt-5 m-0">Property Agents</h4>
            <div class="owl-carousel owl-theme nav-bottom rounded-nav pl-1 pr-1 pt-3 m-0" data-plugin-options="{'items': 1, 'loop': false, 'dots': false, 'nav': true}">
              @foreach ($prop->agents as $agent)                  
                <div class="pr-2 pl-2">
                  <a href="{{ route('front.get.agent.profile', ['slug' => $agent->slug]) }}" class="text-decoration-none">
                    <span class="agent-thumb">
                      <img class="img-fluid rounded-circle" src="{{ getSmallImage(AGENTS_PATH, $agent->img) }}" alt />
                    </span>
                    <span class="agent-infos text-light pt-3">
                      <strong class="text-uppercase font-weight-bold">{{ $agent->name }}</strong>
                      <span class="font-weight-light">{{ $agent->phone }}</span>
                      <span class="font-weight-light">{{ $agent->email }}</span>
                    </span>
                  </a>
                </div>
              @endforeach
            </div>
          </div>

          <h4 class="pt-4 mb-3 text-color-dark">Request Information</h4>
          <p>Contact us or give us a call to request more information</p>

          @include('front.errors.error')

          <form id="contactAgentForm" class="contact-form mb-4" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $agent->id }}">

            <div class="form-row">
              <div class="form-group col">
                <label>Your name *</label>
                <input type="text" value="" minlength="5" maxlength="30" class="form-control" name="name" id="name" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Your email address *</label>
                <input type="email" value="" data-msg-required="Please enter your email address." minlength="5" maxlength="50" class="form-control" name="email" id="email" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Subject</label>
                <input type="text" value="" maxlength="100" class="form-control" name="subject" id="subject">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Message *</label>
                <textarea maxlength="5000" rows="6" class="form-control" name="message" id="message" required></textarea>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <input type="submit" value="Send Message" class="btn btn-secondary mb-lg-5">
              </div>
            </div>
          </form>

        </aside>
      </div>
    </div>

  </div>
@endsection

@section('scripts')
<script>
    $('#nav-props').addClass('active');

    $('#contactAgentForm').submit(function(e){
        e.preventDefault();
        $('#display-errors').hide()
        $('#display-errors ul').empty()
        var formData  = new FormData(jQuery(this)[0]);

        axios.post('{{ route('front.post.agent.sendMessage') }}', {
            agent_id: formData.get('id'),
            name: formData.get('name'),
            email: formData.get('email'),
            subject: formData.get('subject'),
            message: formData.get('message') 
        })
        .then(function (res) {
            Swal.fire(
              'Success!',
              res.data.success,
              'success'
            )

            $('#contactAgentForm').trigger('reset')
        })
        .catch(function (res) {
          let errors = res.response.data.errors
            $('#display-errors').show()
            for (var error in errors) {
                $('#display-errors ul').append('<li class="text-center">'+ errors[error] +'</li>')
            }
        });

  })

</script>
@endsection