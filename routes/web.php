<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Front')->name('front.')->group(function()
{
	Route::get('/','HomeController@index')->name('get.home.index');
	Route::post('/loadMore','HomeController@loadMore')->name('get.home.loadMore');

	Route::get('/properties','PropertyController@index')->name('get.property.index');
	Route::get('/properties/sell','PropertyController@sell')->name('get.property.sell');
	Route::get('/properties/rent','PropertyController@rent')->name('get.property.rent');
	Route::get('/properties/place/{slug}','PropertyController@place')->name('get.property.place');
	
	Route::post('/properties/search','PropertyController@doSearch')->name('post.property.doSearch');
	Route::get('/properties/{slug}','PropertyController@show')->name('get.property.show');

	Route::get('/agents','AgentController@index')->name('get.agent.index');
	Route::get('/agents/profile/{slug}','AgentController@profile')->name('get.agent.profile');
	Route::post('/agents/message','AgentController@sendMessage')->name('post.agent.sendMessage');

	Route::get('/about', 'AboutController@index')->name('get.about.index');

	Route::get('/contact', 'ContactController@index')->name('get.contact.index');
	Route::post('/contact', 'ContactController@sendMessage')->name('post.contact.sendMessage');

	Route::post('/subscribe', 'HomeController@subscribe')->name('post.home.subscribe');
});
